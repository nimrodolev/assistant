import { Body, Controller, Post } from '@nestjs/common';
import { AssistanceTurnRequest } from './models/requests/assistance-turn-request';
import { AssistanceTurnResponse } from './models/responses/assistance-turn-response';
import { AssistantService } from './services/assistant.service';

@Controller('conversation')
export class AppController {
  constructor(private readonly assistantService: AssistantService) {}

  @Post('turn')
  async turn(
    @Body() request: AssistanceTurnRequest,
  ): Promise<AssistanceTurnResponse> {
    const assistanceResult = await this.assistantService.assist(
      request.message,
    );
    return {
      text: assistanceResult.response
        ? assistanceResult.response
        : "Sorry, could you rephrase that? I'm having trouble understanding...",
    };
  }
}
