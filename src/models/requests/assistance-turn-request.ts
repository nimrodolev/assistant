import { IsNotEmpty, IsString } from 'class-validator';

export class AssistanceTurnRequest {
  @IsNotEmpty()
  @IsString()
  message: string;
}
