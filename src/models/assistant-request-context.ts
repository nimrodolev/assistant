export interface AssistantRequestContext {
  clientIP: string;
  clientId?: string;
}
