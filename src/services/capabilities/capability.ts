import { AssistantRequestContext } from '../../models/assistant-request-context';

export type QueryTokens = { [key: string]: string };

export interface Capability {
  readonly name: string;
  readonly isUserAware: boolean;
  execute(
    tokens: QueryTokens,
    context: AssistantRequestContext,
  ): Promise<string>;
}
