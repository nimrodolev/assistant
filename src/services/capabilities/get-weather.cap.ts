import { Injectable } from '@nestjs/common';
import { AssistantRequestContext } from '../../models/assistant-request-context';
import { GeocodeService, Coordinates } from '../geocode.service';
import { GeoIpService } from '../geoip.service';
import { WeatherService } from '../weather.service';
import { Capability, QueryTokens } from './capability';

interface GetWeatherRequest {
  place?: string;
}

@Injectable()
export class GetWeatherCapability implements Capability {
  name = 'GetWeather';
  isUserAware = false;

  constructor(
    private readonly weatherService: WeatherService,
    private readonly geocodeService: GeocodeService,
    private readonly geoIpSerivce: GeoIpService,
  ) {}

  async execute(
    tokens: QueryTokens,
    context: AssistantRequestContext,
  ): Promise<string> {
    const params = this.toWeatherRequest(tokens);
    let location = params.place;
    let coords: Coordinates;
    if (!params.place) {
      const info = await this.geoIpSerivce.getCoordinates(context.clientIP);
      coords = info;
      location = info.city;
    }

    try {
      const {
        current: { temperature, weather_descriptions },
      } = await this.weatherService.get({
        place: params.place,
        coordinates: coords
          ? {
              lat: coords.lat.toString(),
              lon: coords.lon.toString(),
            }
          : undefined,
      });
      return `The weather in ${location} is currently ${weather_descriptions[0].toLowerCase()} with ${temperature} degrees celsius.`;
    } catch (error) {
      return `Sorry, something bad happned. Tell the developers it was a "${error.name}" error`;
    }
  }

  private toWeatherRequest(tokens: QueryTokens): GetWeatherRequest {
    return {
      place: tokens['place'],
    };
  }
}
