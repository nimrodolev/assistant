import { Injectable } from '@nestjs/common';
import { string0To1000 } from 'aws-sdk/clients/customerprofiles';
import * as axios from 'axios';
import { AssistantRequestContext } from 'src/models/assistant-request-context';
import { Capability } from '.';
import { QueryTokens } from './capability';

const ROCKETLAUNCHLIVE_URL =
  'https://fdo.rocketlaunch.live/json/launches/next/1';

interface RocketLaunchLiveResponse {
  result: {
    date_str: string0To1000;
    vehicle: {
      name: string;
    };
    pad: {
      location: {
        name: string;
        country: string;
      };
    };
  }[];
}

@Injectable()
export class GetNextSpaceRoackLaunchCapability implements Capability {
  name = 'GetNextSpaceLaunch';
  isUserAware = false;
  async execute(
    tokens: QueryTokens,
    context: AssistantRequestContext,
  ): Promise<string> {
    const resp = await axios.default.get<RocketLaunchLiveResponse>(
      ROCKETLAUNCHLIVE_URL,
    );
    const {
      result: {
        0: {
          vehicle: { name: vehicle },
          pad: {
            location: { name: pad, country },
          },
          date_str,
        },
      },
    } = resp.data;
    return `The next space launch will be the ${vehicle} rocket, which will be launching from ${pad}, ${country}, on ${date_str}`;
  }
}
