import { Inject, Injectable, Scope } from '@nestjs/common';
import { getConfig } from '../config';
import { Capability } from '../services/capabilities/';
import { QueryParsingService } from './query-parsing.service';
import { REQUEST } from '@nestjs/core';
import { request, Request } from 'express';

const config = getConfig();

export enum ASSISTANCE_ERRORS {
  no_intent_detected,
  unknown_intent,
}

export interface AssistanceResult {
  succeeded: boolean;
  response?: string;
  error?: ASSISTANCE_ERRORS;
}

@Injectable({ scope: Scope.REQUEST })
export class AssistantService {
  constructor(
    @Inject(REQUEST) private readonly request: Request,
    @Inject('Capabilities') private readonly capabilities: Capability[],
    private readonly queryParser: QueryParsingService,
  ) {}

  public async assist(query: string): Promise<AssistanceResult> {
    const parsingResult = await this.queryParser.parse({ query });
    if (!parsingResult.intent) {
      return {
        succeeded: false,
        error: ASSISTANCE_ERRORS.no_intent_detected,
        response: parsingResult.message,
      };
    }
    const capability = this.capabilities.find((c) => {
      return c.name === parsingResult.intent;
    });

    if (!capability) {
      return {
        succeeded: false,
        error: ASSISTANCE_ERRORS.unknown_intent,
      };
    }

    const response = await capability.execute(parsingResult.tokens, {
      clientIP: this.request.connection.remoteAddress,
    });
    return {
      response,
      succeeded: true,
    };
  }
}
