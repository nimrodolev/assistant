import { Injectable } from '@nestjs/common';
import { AWSError, LexRuntime } from 'aws-sdk';
import * as uuid from 'uuid';
import { getConfig } from '../config';

const config = getConfig();

export interface QueryParsingRequest {
  query: string;
  userId?: string;
}

export interface QueryParsingResult {
  intent: string;
  tokens: LexRuntime.StringMap;
  message?: string;
}

@Injectable()
export class QueryParsingService {
  async parse(request: QueryParsingRequest): Promise<QueryParsingResult> {
    const runtime = new LexRuntime({
      credentials: {
        accessKeyId: config.aws.accessKeyId,
        secretAccessKey: config.aws.secretAccessKey,
      },
      region: config.aws.region,
    });

    return new Promise((resolve, reject) => {
      runtime.postText(
        {
          botName: config.aws.lex.botName,
          inputText: request.query,
          userId: request.userId || uuid.v4(),
          botAlias: config.aws.lex.alias,
        },
        (error: AWSError, data: LexRuntime.PostTextResponse) => {
          if (error) {
            return reject(error);
          }
          return resolve({
            intent: data.intentName,
            tokens: data.slots,
            message: data.message,
          });
        },
      );
    });
  }
}
