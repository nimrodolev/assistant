import { Injectable } from '@nestjs/common';
import * as axios from 'axios';
import { getConfig } from '../config';

const config = getConfig();

const WEATHERSTACK_URL = 'http://api.weatherstack.com/';

export interface WeatherQuery {
  place?: string;
  coordinates?: {
    lat: string;
    lon: string;
  };
}

interface WeatherStackResponse {
  current: {
    temperature: number;
    weather_icons: string[];
    weather_descriptions: string[];
    wind_speed: number;
    humidity: number;
    feelslike: number;
  };
}

@Injectable()
export class WeatherService {
  private client: axios.AxiosInstance;

  constructor() {
    this.client = axios.default.create({
      baseURL: WEATHERSTACK_URL,
      params: {
        access_key: config.services.weatherstack.apiKey,
      },
    });
  }

  async get(query: WeatherQuery): Promise<WeatherStackResponse> {
    if (!query.coordinates && !query.place) {
      throw new Error(
        'Invalid request - must specify either coordinates or a place name',
      );
    }
    const resp = await this.client.get<WeatherStackResponse>('current', {
      params: {
        query: query.place
          ? query.place
          : `${query.coordinates.lat},${query.coordinates.lon}`,
        units: 'm',
      },
    });
    return resp.data;
  }
}
