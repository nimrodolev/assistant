import { Injectable } from '@nestjs/common';
import * as axios from 'axios';
import { getConfig } from '../config';

const config = getConfig();

const MAPBOX_URL = 'https://api.mapbox.com/geocoding/v5/mapbox.places';

interface MapboxResponse {
  features: MapboxFeature[];
}

interface MapboxFeature {
  place_name: string;
  geometry: {
    coordinates: {
      ['0']: number;
      ['1']: number;
    };
  };
}

export interface Coordinates {
  lat: number;
  lon: number;
}

@Injectable()
export class GeocodeService {
  private client: axios.AxiosInstance;

  constructor() {
    this.client = axios.default.create({
      baseURL: MAPBOX_URL,
      params: {
        access_token: config.services.mapbox.apiKey,
      },
    });
  }

  async getCoordinates(location: string): Promise<Coordinates> {
    const resp = await this.client.get<MapboxResponse>(`${location}.json`, {
      params: {
        mapobxLimit: 1,
        mapboxType: 'place',
      },
    });

    if (!resp.data?.features?.length) {
      throw new Error(`Failed to get geocode info for query "${location}"`);
    }
    const coords = resp.data.features[0].geometry.coordinates;
    return {
      lat: coords[0],
      lon: coords[1],
    };
  }
}
