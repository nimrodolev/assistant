import { Injectable } from '@nestjs/common';
import * as axios from 'axios';
import { getConfig } from '../config';

const config = getConfig();

const LOCAL_IP_V4 = '127.0.0.1';
const LOCAL_IP_V6 = '::1';
const IPSTACK_URL = 'http://api.ipstack.com/';
const NOIP_URL = 'http://ip1.dynupdate.no-ip.com/';

export interface LocationInfo {
  lat: number;
  lon: number;
  city: string;
}

interface IpStackResponse {
  latitude: number;
  longitude: number;
  city: string;
}

@Injectable()
export class GeoIpService {
  async getCoordinates(ip: string): Promise<LocationInfo> {
    if (ip.includes(LOCAL_IP_V4) || ip === LOCAL_IP_V6) {
      ip = await this.getMyIP();
    }
    const resp = await axios.default.get<IpStackResponse>(
      `${IPSTACK_URL}/${ip}?access_key=${config.services.ipstack.apiKey}`,
    );
    return {
      lat: resp.data.latitude,
      lon: resp.data.longitude,
      city: resp.data.city,
    };
  }

  private async getMyIP(): Promise<string> {
    const resp = await await axios.default.get(NOIP_URL);
    return resp.data as string;
  }
}
