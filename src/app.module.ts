import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { QueryParsingService } from './services/query-parsing.service';
import {
  GetNextSpaceRoackLaunchCapability,
  GetWeatherCapability,
} from './services/capabilities';
import { GeocodeService } from './services/geocode.service';
import { GeoIpService } from './services/geoip.service';
import { WeatherService } from './services/weather.service';
import { AssistantService } from './services/assistant.service';

const capabilitiesProvider = {
  provide: 'Capabilities',
  useFactory: (...args: any[]) => [...args],
  inject: [GetWeatherCapability, GetNextSpaceRoackLaunchCapability],
};

@Module({
  imports: [],
  controllers: [AppController],
  providers: [
    QueryParsingService,
    GeocodeService,
    GeoIpService,
    WeatherService,
    AssistantService,
    capabilitiesProvider,
    ...capabilitiesProvider.inject,
  ],
})
export class AppModule {}
