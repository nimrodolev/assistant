import { AppConfig } from './app-config';
import * as dev from './config.dev';
import * as prod from './config.prod';

export const getConfig = (): AppConfig => {
  if (process.env.DEVELOPMENT) {
    return dev.config;
  }
  return prod.config;
};
