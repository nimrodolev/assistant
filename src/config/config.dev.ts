import { AppConfig } from './app-config';

export const config: AppConfig = {
  aws: {
    accessKeyId: process.env.AWS__ACCESSKEYID,
    secretAccessKey: process.env.AWS__SECRETACCESSKEY,
    region: 'eu-west-1',
    lex: {
      botName: 'Assistant',
      alias: 'development',
    },
  },
  server: {
    port: 3000,
  },
  services: {
    ipstack: {
      apiKey: process.env.SERVICES__IPSTACK__APIKEY,
    },
    weatherstack: {
      apiKey: process.env.SERVICES__WEATHERSTACK__APIKEY,
    },
    mapbox: {
      apiKey: process.env.SERVICES__MAPBOX__APIKEY,
    },
  },
};
