export interface AppConfig {
  aws: {
    accessKeyId: string;
    secretAccessKey: string;
    region: string;
    lex: {
      botName: string;
      alias: string;
    };
  };
  server: {
    port: number;
  };
  services: {
    ipstack: {
      apiKey: string;
    };
    weatherstack: {
      apiKey: string;
    };
    mapbox: {
      apiKey: string;
    };
  };
}
