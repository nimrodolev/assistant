import { AppConfig } from './app-config';

export const config: AppConfig = {
  aws: {
    accessKeyId: '',
    secretAccessKey: '',
    region: 'eu-west-1',
    lex: {
      botName: 'Assistant',
      alias: 'production',
    },
  },
  server: {
    port: 80,
  },
  services: {
    ipstack: {
      apiKey: process.env.SERVICES__IPSTACK__APIKEY,
    },
    weatherstack: {
      apiKey: process.env.SERVICES__WEATHERSTACK__APIKEY,
    },
    mapbox: {
      apiKey: process.env.SERVICES__MAPBOX__APIKEY,
    },
  },
};
