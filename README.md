## Description

A sample project that provides an API for assistant-like functionality.

## Project

A [NestJS](https://nestjs.com/) Typescript project with one controller accepting a simple `POST` request with the user's question (or any message really) and returning the text to be presented to the user.



## Submission info

I didn't really time myself, but after about 4 hours of work I stopped to make sure everything is basically working and not broken in an obvious way and [committed the work I had ready](https://gitlab.com/nimrodolev/assistant/-/tree/04271a6ddf700781836632bff25a1b92a5a4ae9e), so you guys can evaluate based on where I was after the "allotted" time, if you prefer. I do want to add a bit more functionality, which I will do as a bit of extra work outside of the 4 hours.

I built the intent recognition and token extraction models manually using AWS Lex. I had no experience with Lex before this exercise and have just read about it, but as is AWS's brand they made everything very simple.

Here's an example of how the Lex setup looks - 

![](./AWS-Lex-Screenshot.png)

And an example export JSON file can be found [here](./Assistant_Export.json);

## Running locally

You will need keys for the APIs and everything - I'm happy to provide a `.env` file with everything via email - just send me a message and ask.

```bash
# make sure to get all the dependancies
$ npm ci

# development
$ npm run start

# watch mode
$ npm run start:dev
```

Once the server is running you should be able to execute a call:


```javascript
// POST http://localhost:3000/conversation/turn/
{
    "message": "What's the weather in Madagascar?"
}
```
